const fetch = require("node-fetch");

async function fetchingTodos(chunkIds){
  const promise=await chunkIds.map((userId) =>{
    return fetch(`http://localhost:3000/todos?user_id=${userId}`)
          .then((response)=> response.json())
  })
  const chunkTodos=await Promise.all(promise);
  const todosData=chunkTodos.map((allTodos)=>{
    const index=allTodos.todos[0].text.split(" ");
    const userId=index[4]
    const noOfTodosCompleted=allTodos.todos.filter((todo)=> todo.isCompleted).length;
    let userData={
      id:parseInt(userId),
      name:"User "+userId,
      numTodosCompleted:noOfTodosCompleted
    }
    
   return userData
    
 })
 return (todosData);
 
}


async function main() {
  const response = await fetch("http://localhost:3000/todos?user_id=2");
  const data = await response.json();
  const allUser= await fetch("http://localhost:3000/users")
  const usersData=await allUser.json()
  const allUsersWithNumberOfTodosCompleted=[]
  let startIndex=0;
  const endIndex=5;
  
  while (startIndex <usersData.users.length){
    const chunkUsersIds=usersData.users.slice(startIndex,endIndex+startIndex).map((user)=>user.id);
    let userWithNumberOfTodoCompleted=await fetchingTodos(chunkUsersIds);
    
    allUsersWithNumberOfTodosCompleted.push(userWithNumberOfTodoCompleted)
    startIndex+=5 ;
    await new Promise((resolve) =>setTimeout(resolve,1000));
    
  }
  console.log(allUsersWithNumberOfTodosCompleted);
  
}
main();

